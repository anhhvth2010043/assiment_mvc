﻿using Assment_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assment_MVC.Controllers
{
    public class HomeController : Controller
    {
        AssimentEntities ass = new AssimentEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult CreateProduct()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        
        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StoreCategory(Category model)
        {
            Category category = new Category();
            if (ModelState.IsValid)
            {
                category.name = model.name;

               
            }
            ass.Categories.Add(category);
            ass.SaveChanges();
            ModelState.Clear();
            return Redirect("CreateCategory");
        }
    }
}