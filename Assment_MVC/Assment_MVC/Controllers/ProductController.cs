﻿using Assment_MVC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assment_MVC.Controllers
{
    public class ProductController : Controller
    {

        AssimentEntities db = new AssimentEntities();

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetCategoryKey()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = db.Categories.ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetProduct()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = db.Products.SqlQuery("Select * from Product");
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddProduct(Product model)
        {
            db.Products.Add(model);
            try {
                db.SaveChanges();
                ModelState.Clear();
                return Json("success", JsonRequestBehavior.AllowGet);
            } catch (Exception ex) {
                Debug.WriteLine(ex);
                return Json("false!!", JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpdateProduct(Product product)
        {
            try {
                var data = db.Products.Where(x => x.product_id == product.product_id).FirstOrDefault();
                if(data!= null)
                {
                    data.product_name = product.product_name;
                    data.product_price = product.product_price;
                    data.catrgory_id = product.catrgory_id;
                    db.SaveChanges();
                }
                return Json(new { 
                    status = true,
                    message = "true"
                });
            }   
            catch (Exception ex) {
                return Json(new {
                    status = false,
                    message = ex
                });
            }
        }


        [HttpGet]
        public JsonResult GetProductById(int id)
        {
            var result = db.Products.Where(x => x.product_id == id ).SingleOrDefault();
            string values = string.Empty;
            values = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(values, JsonRequestBehavior.AllowGet);
        }
        

        [HttpDelete]
        public JsonResult Delete(int id)
        {
            return Json("success", JsonRequestBehavior.AllowGet);
        }


        public JsonResult RemoveProuct(int id)
        {
            var res = db.Products.Where(x => x.product_id == id).First();
            db.Products.Remove(res);
            db.SaveChanges();
            return Json("success", JsonRequestBehavior.AllowGet);
        }
    }
}