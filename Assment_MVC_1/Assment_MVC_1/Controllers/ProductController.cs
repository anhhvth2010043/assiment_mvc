﻿using Assment_MVC_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assment_MVC_1.Controllers
{
    public class ProductController : Controller
    {

        Example_1Entities db = new Example_1Entities();
        // GET: Product
        public ActionResult Index(tbl_Product pro)
        {
            if (pro != null)
            {
                return View(pro);
            }
            else
            {
                return View();
            }
        }

       
        public ActionResult AddProduct()
        {
            return View("AddProduct");
        }
        [HttpPost]
        public ActionResult CreateProduct(tbl_Product model)
        {
            tbl_Product pro = new tbl_Product();
            if (ModelState.IsValid)
            {
                pro.ProductName = model.ProductName;
                pro.CategoryID = model.CategoryID;
                pro.SupplierlId = model.SupplierlId;
                pro.QuantityPerUnit = model.QuantityPerUnit;
                pro.UnitPrice = model.UnitPrice;
                pro.UnitsOnOrder = model.UnitsOnOrder;
                pro.UnitslnStock = model.UnitslnStock;
                pro.ReorderLevel = model.ReorderLevel;
                pro.Discontinued = model.Discontinued;
            }
            db.tbl_Product.Add(pro);
            db.SaveChanges();
            ModelState.Clear();
            return View("");
          
        }

        public ActionResult StudentList()
        {
            var res = db.tbl_Product.ToList();
            return View(res);
        }

        public ActionResult Delete(int id)
        {
            var res = db.tbl_Product.Where(x => x.ProductID == id).First();
            db.tbl_Product.Remove(res);
            db.SaveChanges();

            var list = db.tbl_Product.ToList();
            return View("AddProduct", list);
        }
    }
}