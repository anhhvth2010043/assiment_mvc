﻿using Assment_MVC_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assment_MVC_1.Controllers
{
    public class HomeController : Controller
    {
        Example_1Entities db = new Example_1Entities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Category()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult CreateCategory(tbl_Category model)
        {
            tbl_Category category = new tbl_Category();

            if (ModelState.IsValid)
            {
                category.CategoryName = model.CategoryName;
                category.Description = model.CategoryName;
                category.Picture = model.Picture;
            }
            db.tbl_Category.Add(category);
            db.SaveChanges();
            ModelState.Clear();
            return View("Category");
        }
    }
}